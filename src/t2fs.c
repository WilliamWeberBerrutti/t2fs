#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/apidisk.h"
#include "../include/bitmap2.h"
#include "../include/t2fs.h"

/// Constantes do programa
#define SUCCESS 0    /// função terminou com sucesso
#define FOUND 1      /// função encontrou um elemento
#define NOTFOUND -1 /// função não encontrou um elemento
#define ERROR -1     /// função executou incorretamente
#define FAILURE -1   /// o sistema entrou em "pane", em uma situação fatal
#define INVALID -1   /// informação inválida


/// definicoes sobre arquivos (A CHECAR!!!)
#define MAX_FILES 20
#define FILESIZE 32		/// numero máx. de caracteres no nome do arquivo (contando o '\0')
#define DATAFILE 1
#define DIRECTORY 0

/// tipo do arquivo
#define FILE_INVALID 0
#define FILE_VALID 1
#define FILE_DIR 2

/// Pus 20 para testar
#define MFTSIZE 4096//2048//2048//10//2048*1024/32	/// nº blocos MFT * BlockSize / tam. struct MFT
#define MFT_REGISTRY_AMOUNT 32

/// Limites de blocos iniciais das seções do disco virtual
#define MFT_START 1
#define DATA_START 2049
#define MFT_FREE 0
#define MFT_ALLOCATED 1

/// Arquivos
#define MFT_EMPTY -1	/// registro MFT livre
#define MFT_END 0		/// marcador de fim de encadeamento
#define MFT_MAP 1		/// tupla de mapeamento VBN - LBN
#define MFT_ADD 2		///	registro MFT adicional

/// Bitmap
#define BITMAP_FREE 0		/// bit livre
#define BITMAP_ALLOCATED 1	/// bit alocado

/// Variáveis Globais
FILE2 globalFileHandle = 1;		/// é o ID dos arquivos
FILE2 countOpenFiles = 0;
FILE2 countMFT = 0;
FILE2 currentDir = 0;

/// Estruturas do programa
/// (FILE2 é um alias para int)
struct t2fs_bootBlock BootBlock;	/// Bloco de Boot (com as definições de tamanho de bloco, versão do sistema, etc.
struct t2fs_record Registry;		/// Registro de um arquivo no disco
struct t2fs_4tupla RegMFT[MFT_REGISTRY_AMOUNT];

int MFT[MFTSIZE];	/// Master File Table (precisa?).

/// Definição do descritor de arquivos abertos
typedef struct
{
	struct t2fs_record record;
	FILE2 handle;
	int currentPointer;
	DIRENT2 directory;
} DESCRIPTOR;

DESCRIPTOR openedFiles[MAX_FILES];


/***** Funções do sistema (REMOVER TODOS OS PROTÓTIPOS, EXCETO OS DAS FUNÇÕES AUXILIARES, ANTES DE ENTREGAR!) *****/


/// Prontas

/// Auxiliares
void initBootBlock();
void initMFT();
void initRegMFT();
void initOpenedFiles();
void DEBUG_PrintT2FS();
void DEBUG_PrintMFT();
void DEBUG_PrintRegMFT();
void DEBUG_PrintOpenedFiles();
int searchDescHandle(FILE2 handle);
int returnIndexByHandle(FILE2 handle);
int searchOpenFiles(char *filename);
int MFTSearchFree();
int MFTCheck();

/// Do Trabalho
int identify2 (char *name, int size);

/// A fazer
/**
 * IDEIA: focar nas funções de arquivo antes de ir para as de diretório. Pensar em diretório de 1 nível para facilitar
 * Motivo: se entendermos o funcionamento para arquivos, acho que será mais fácil de fazer para diretórios (que são tipos especiais de arquivos)
 * */

FILE2 create2 (char *filename);
int delete2 (char *filename);
FILE2 open2 (char *filename);
int close2 (FILE2 handle);
int read2 (FILE2 handle, char *buffer, int size);
int write2 (FILE2 handle, char *buffer, int size);
int truncate2 (FILE2 handle);
int seek2 (FILE2 handle, DWORD offset);
int mkdir2 (char *pathname);
int rmdir2 (char *pathname);
DIR2 opendir2 (char *pathname);
int readdir2 (DIR2 handle, DIRENT2 *dentry);
int closedir2 (DIR2 handle);


/// Implementações das Funções

/***** FUNÇÕES AUXILIARES *****/

void initBootBlock()
{
	unsigned char buffer[SECTOR_SIZE];

	read_sector(0, buffer);

	memcpy(&BootBlock,buffer, SECTOR_SIZE);

	return;
}


void initMFT()
{
	int i;

	for (i = 0; i < MFTSIZE; i++)
	{
		MFT[i] = MFT_FREE;
	}
}


void initRegMFT()
{
	int i;

	for (i = 0; i < MFT_REGISTRY_AMOUNT; i++)
	{
		RegMFT[i].atributeType = MFT_EMPTY;
		RegMFT[i].virtualBlockNumber = INVALID;
		RegMFT[i].logicalBlockNumber = INVALID;
		RegMFT[i].numberOfContiguosBlocks = INVALID;
	}
}

void initOpenedFiles()
{
    int i;

    for (i = 0; i < MAX_FILES; i++)
    {
        openedFiles[i].record.TypeVal = 0;
        strcpy(openedFiles[i].record.name, "n/a");
        openedFiles[i].record.blocksFileSize = 0;
        openedFiles[i].record.bytesFileSize = 0;
    }
}

void DEBUG_PrintT2FS()
{
    initBootBlock();

	int i;

	printf("\nID: ");
	for (i = 0; i < 4; i++)
	    printf("%c", BootBlock.id[i]);
    printf("\n");
	printf("VERSION: 0x%04x \n", BootBlock.version);
	printf("BLOCKSIZE: 0x%04x \n", BootBlock.blockSize);
	printf("MFTBLOCKSSIZE: 0x%04x \n", BootBlock.MFTBlocksSize);
	printf("DISKSECTORSIZE: 0x%04x \n", BootBlock.diskSectorSize);
}

void DEBUG_PrintMFT()
{
    //initMFT();
	printf("\n--- DEBUG PRINT MFT ---\n");
	int i;
	for (i = 4; i < MFTSIZE; i++)
	{
		unsigned char buffer[SECTOR_SIZE];
		read_sector(i,buffer);
		if(buffer[0] == 1){
			memcpy(&RegMFT,buffer, SECTOR_SIZE);
			printf("\nMFT %d - Setor do disco: %d\n",(i/2 - 2) ,i);
			printf("AtributeType: %d\n", RegMFT[0].atributeType);
			printf("VirtualBlockNumber: %d\n", RegMFT[0].virtualBlockNumber);
			printf("LogicalBlockNumber: %d\n", RegMFT[0].logicalBlockNumber);
			printf("NumberOfContiguosBlocks: %d\n", RegMFT[0].numberOfContiguosBlocks);
		}
	}
}

void DEBUG_PrintRegMFT()
{
    //initRegMFT();

	int i;
	for (i = 0; i < MFT_REGISTRY_AMOUNT; i++)
	{
		printf("ID: %d\n", i);

		printf("AtributeType: %d \n", RegMFT[i].atributeType);
		printf("VirtualBlockNumber: %d \n", RegMFT[i].atributeType);
		printf("LogicalBlockNumber: %d \n", RegMFT[i].logicalBlockNumber);
		printf("NumberOfContiguosBlocks: %d \n", RegMFT[i].numberOfContiguosBlocks);
	}
}

void DEBUG_PrintOpenedFiles()
{
    int i;
    struct t2fs_record reg;

	initOpenedFiles();

    for (i = 0; i < MAX_FILES; i++)
    {
        reg = openedFiles[i].record;
        printf("\nI: %d\n", i);
        printf("TypeVal: %d\n", reg.TypeVal);
        printf("Name: %s\n", reg.name);
        printf("Reserved: -- \n");
        printf("BlocksFileSize: %d\n", reg.blocksFileSize);
        printf("BytesFileSize: %d\n", reg.bytesFileSize);
    }
}

/// Procura um arquivo aberto em 'openedFiles', usando o handle, e retorna seu handle, caso encontre
int searchDescHandle(FILE2 handle)
{
    int i;
	for (i = 0; i < MAX_FILES; i++)
	{
		if (openedFiles[i].handle == handle)
		{
			return handle;	/// encontrou o handle, retorna ele
		}
	}
	return ERROR;	/// não encontrou o handle
}

/// Procura um arquivo aberto em 'openedFiles', usando o handle, e retorna seu índice, caso encontre
int returnIndexByHandle(FILE2 handle)
{
    int i;
	for (i = 0; i < MAX_FILES; i++)
	{
		if (openedFiles[i].handle == handle)
		{
			return i;	/// encontrou o handle, retorna o índice dele
		}
	}
	return ERROR;	/// não encontrou o handle
}

/// Procura um arquivo aberto em 'openedFiles', usando o nome do arquivo, e retorna seu handle, caso encontre
int searchOpenFiles(char *filename)
{
    struct t2fs_record *reg;

	int i;
	for (i = 0; i < MAX_FILES; i++)
	{
        reg = &(openedFiles[i].record);
		if (strcmp (reg->name, filename) == 0)	/// se o arquivo ja esta aberto
		{
			return openedFiles[i].handle;	/// encontrou o arquivo
		}
	}
	return ERROR;	/// não encontrou o arquivo
}

int MFTSearchFree()
{
		unsigned char buffer[SECTOR_SIZE];
    int i;

		for (i = 4; i < MFTSIZE; i++)
		{
			read_sector((i*2 + 4),buffer);
			if(buffer[0] == 255){ //255 É MFT LIVRE! PRECISA MUDAR O VALOR DA CONSTANTE
				return i;
    	}
		}
  return ERROR;
}

int MFTCheck(){

	int i = 0;

	unsigned char buffer[256];

	for(i = 4; i < MFTSIZE; i++){
		read_sector(i,buffer);
		if(buffer[0] == 1){
			printf("BUFFER: %s\n", buffer);
			printf("SETOR: %d\n", i);
		}

	}

	return SUCCESS;

}

/***** FUNÇÕES DO TRABALHO *****/

int identify2 (char *name, int size)
{
	if (size >= 49)
	{
		puts(name);
		return SUCCESS;
	}
	else
	{
		return ERROR;
	}
}

/*-----------------------------------------------------------------------------
Função: Criar um novo arquivo.
	O nome desse novo arquivo é aquele informado pelo parâmetro "filename".
	(OK) O contador de posição do arquivo (current pointer) deve ser colocado na posição zero.
	(OK) Caso já exista um arquivo ou diretório com o mesmo nome, a função deverá retornar um erro de criação.
	(OK) A função deve retornar o identificador (handle) do arquivo.
	Esse handle será usado em chamadas posteriores do sistema de arquivo para fins de manipulação do arquivo criado.

Entra:	filename -> path absoluto para o arquivo a ser criado. Todo o "path" deve existir.

Saída:	Se a operação foi realizada com sucesso, a função retorna o handle do arquivo (número positivo).
	Em caso de erro, deve ser retornado um valor negativo.
-----------------------------------------------------------------------------*/
/// Definição do descritor de arquivos abertos
/*typedef struct
{
	struct t2fs_record record;
	FILE2 handle;
	int currentPointer;
	DIRENT2 directory;
} DESCRIPTOR;

DESCRIPTOR openedFiles[MAX_FILES];*/

/// FALTA TESTAR
/**
  Faltou:
  - ler do bloco físico do disco
**/

FILE2 create2 (char *filename)
{
	int fileCreationStatus = 0;	/// contador que define o estado de um arquivo. Se for uma criação bem-sucedida, ele deve conter o valor 3, indicando que conseguiu inicializar o bitmap, alocar o bloco do disco virtual, e inicializar o descritor de arquivo
	int status_searchfile = searchOpenFiles(filename);

	if (status_searchfile == NOTFOUND)	/// se não encontrou nenhum arquivo com o mesmo nome, então pode abrir ele
	{
		/// alocar no bitmap
		int blockNumber = searchBitmap2(BITMAP_FREE);	/// procura no bitmap uma posição livre

		if (blockNumber != ERROR)		/// se encontrou um bitmap livre, sem erros, então prepara o bitmap
		{
			if (setBitmap2 (blockNumber, BITMAP_ALLOCATED) == SUCCESS)
			{
				printf("Inc 1\n");
				fileCreationStatus++;
			}
		}

		/// alocar no disco virtual
		int sector;
		int writecount = 0;

		for (sector = 0; sector < 4; sector++)
		{
			if (write_sector (blockNumber*4+sector, (unsigned char*)"0") == SUCCESS)
			{
				writecount++;
			}
		}

		if (writecount == 4)
		{
			printf("Inc 2\n");

			fileCreationStatus++;
		}

		/// Escrever no MFT
		/// escrever este registro no disco, e como guardar a informação de onde vai estar isso.
		Registry.TypeVal = FILE_VALID;
		strcpy(Registry.name, filename);
		Registry.blocksFileSize = 1;
		Registry.bytesFileSize = 0;
		Registry.MFTNumber = MFTSearchFree();

		//
		MFT[Registry.MFTNumber] = MFT_ALLOCATED;

		initRegMFT();

		RegMFT[0].atributeType = 1;
		RegMFT[0].virtualBlockNumber = 0;
		RegMFT[0].logicalBlockNumber = blockNumber;
		RegMFT[0].numberOfContiguosBlocks = 1;

		int sectorWrite;

		sectorWrite = 2 * Registry.MFTNumber + 4;

		if (write_sector(sectorWrite, &RegMFT) == SUCCESS)
		{
			printf("Inc 3\n");

			fileCreationStatus++;
		}
		//
		/*if (write_sector(RegMFT[Registry.MFTNumber]) == SUCCESS)
		{
			fileCreationStatus++;
		}*/

		/// inicializa o descritor de arquivos apropriadamente
		openedFiles[countOpenFiles].currentPointer = 0;
		openedFiles[countOpenFiles].handle = globalFileHandle;
		globalFileHandle++;
		countOpenFiles++;
		fileCreationStatus++;
		printf("Inc 4\n");

		printf("fileCreationStatus = %d\n", fileCreationStatus );

	}

	if (fileCreationStatus == 4)	/// conseguiu alterar o bitmap, o disco virtual, o MFT e o descritor de arquivos, então a operação foi bem-sucedida
		return openedFiles[countOpenFiles-1].handle;
	else	/// senão, não foi
		return ERROR;

}

/*
typedef struct
{
	struct t2fs_record record;
	FILE2 handle;
	int currentPointer;
	DIRENT2 directory;
} DESCRIPTOR;

struct t2fs_record {
    BYTE    TypeVal;
    char    name[MAX_FILE_NAME_SIZE];
    DWORD   blocksFileSize;
    DWORD   bytesFileSize;
    DWORD   MFTNumber;
};

typedef struct {
    char    name[MAX_FILE_NAME_SIZE+1];
    BYTE    fileType;
    DWORD   fileSize;
} DIRENT2;

DESCRIPTOR openedFiles[MAX_FILES];
*/

/**
 PASSO-A-PASSO PARA O DELETE2:

 1) procurar o arquivo no diretório
 2) se encontrou ele, zerar seus campos, e retornar SUCCESS
 3) senão, retornar ERROR
* */
int delete2 (char *filename)
{
    return SUCCESS;
}

FILE2 open2 (char *filename)
{
	/// ler do disco todas as informações abaixo
	// read_sector();
	/*openedFiles[countOpenFiles].record.TypeVal = 0;
	openedFiles[countOpenFiles].record.name = NULL;
	openedFiles[countOpenFiles].record.blocksFileSize = 0;
	openedFiles[countOpenFiles].record.bytesFileSize = 0;
	openedFiles[countOpenFiles].record.MFTNumber = 0;
	openedFiles[countOpenFiles].handle = 0;
	openedFiles[countOpenFiles].currentPointer = 0;	/// aqui está certo!
	openedFiles[countOpenFiles].directory.name = NULL;
	openedFiles[countOpenFiles].directory.fileType = 0;
	openedFiles[countOpenFiles].directory.fileSize = 0;*/
	countOpenFiles++;

    return SUCCESS;
}

/// FALTA TESTAR
int close2 (FILE2 handle)
{
	int i, posicao = INVALID;

	for(i = 0; i < MAX_FILES; i++)
	{
        if(openedFiles[i].handle == handle)
        {
            posicao = i;
            break;
        }
	}
	if(posicao == INVALID)
        return ERROR;

    for(i = posicao; i < MAX_FILES-1; i++)
    {
        openedFiles[i] = openedFiles[i+1];
    }
    countOpenFiles--;

	return SUCCESS;
}

/*-----------------------------------------------------------------------------
Função:	Realiza a leitura de "size" bytes do arquivo identificado por "handle".
	Os bytes lidos são colocados na área apontada por "buffer".
	Após a leitura, o contador de posição (current pointer) deve ser ajustado para o byte seguinte ao último lido.

Entra:	handle -> identificador do arquivo a ser lido
	buffer -> buffer onde colocar os bytes lidos do arquivo
	size -> número de bytes a serem lidos

Saída:	Se a operação foi realizada com sucesso, a função retorna o número de bytes lidos.
	Se o valor retornado for menor do que "size", então o contador de posição atingiu o final do arquivo.
	Em caso de erro, será retornado um valor negativo.
-----------------------------------------------------------------------------*/
/**
 PASSO-A-PASSO PARA A READ2:
 1) Verificar no descritor de arquivos a existência deste handle
 2)

*/
int read2 (FILE2 handle, char *buffer, int size)
{
	// procurar arquivo

	//

	/*int bytes;

	for (bytes = 0; bytes <= size; bytes += 256)
	{
		read_sector (search_sector, buffer);
	}*/

	// ajustar o currentPointer para bytes+1 (?)

    return SUCCESS;
}

int write2 (FILE2 handle, char *buffer, int size)
{
    return SUCCESS;
}

int truncate2 (FILE2 handle)
{
    return SUCCESS;
}

/// FALTA TESTAR
int seek2 (FILE2 handle, DWORD offset)
{
    int idx = returnIndexByHandle(handle);

    if(idx == ERROR)
    {
        return ERROR;
    }

    openedFiles[idx].currentPointer = offset;

    return SUCCESS;
}

int mkdir2 (char *pathname)
{
    return SUCCESS;
}

int rmdir2 (char *pathname)
{
    return SUCCESS;
}

DIR2 opendir2 (char *pathname)
{
    return SUCCESS;
}

int readdir2 (DIR2 handle, DIRENT2 *dentry)
{
    return SUCCESS;
}

int closedir2 (DIR2 handle)
{
    return SUCCESS;
}
