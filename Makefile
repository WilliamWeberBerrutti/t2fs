CC=gcc
LIB_DIR=./lib
INC_DIR=./include
BIN_DIR=./bin
SRC_DIR=./src
TST_DIR=./teste

all: t2fs.o libt2fs.a test1 #test2 test3

libt2fs.a: t2fs.o
	ar crs libt2fs.a $(LIB_DIR)/t2fs.o $(LIB_DIR)/apidisk.o $(LIB_DIR)/bitmap2.o
	mv libt2fs.a $(LIB_DIR)

t2fs.o:
	$(CC) -c $(SRC_DIR)/t2fs.c -Wall
	mv t2fs.o $(LIB_DIR)

test1: $(TST_DIR)/teste1.c $(LIB_DIR)/libt2fs.a
	$(CC) -o teste1 $(TST_DIR)/teste1.c -L$(LIB_DIR) -lt2fs -Wall
	
clean:
	rm -r $(LIB_DIR)/t2fs.o $(LIB_DIR)/libt2fs.a teste1
